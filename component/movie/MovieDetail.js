import React, {Component} from 'react';
import {Text, View, Image,ActivityIndicator,ScrollView} from 'react-native';

export default class MovieDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies:[],
      isloading:true
    };
  }
  componentDidMount(){
      const data = require('../testdata/movie.json')   
      setTimeout(() => {
        this.setState({
          movies: data,
          isloading:false
        });
      }, 1000);
  }
  render() {
      if(this.state.isloading) {
          return (
            <ActivityIndicator color="#FB7299" size="large"></ActivityIndicator>
          );
      }
    return (
      <ScrollView>
        <View style={{padding: 10}}>
          <Text
            style={{
              fontSize: 25,
              textAlign: 'center',
              marginTop: 20,
              marginBottom: 20,
            }}>
            {this.state.movies.name}
          </Text>
          <View style={{alignItems: 'center'}}>
            <Image
              source={{uri: this.state.movies.image}}
              style={{width: 200, height: 280}}></Image>
          </View>
          <View>
            <Text style={{lineHeight: 30, marginTop: 20}}>
              {this.state.movies.description}
            </Text>
            <Text style={{fontSize: 25}}>导演</Text>
            {this.state.movies.director.map((item, i) => {
              return <Text key={i}>{item.name}</Text>;
            })}
            <Text style={{fontSize: 25}}>作者</Text>
            {this.state.movies.author.map((item, i) => {
              return <Text key={i}>{item.name}</Text>;
            })}
            <Text style={{fontSize: 25}}>演员</Text>
            {this.state.movies.actor.map((item, i) => {
              return <Text key={i}>{item.name}</Text>;
            })}
          </View>
        </View>
      </ScrollView>
    );
  }
}
