// import React, {Component} from 'react';
// import {Text, View} from 'react-native';
// export default class Search extends Component {
//   render() {
//     return (
//       <View style={{alignItems: 'center'}}>
//         <Text style={{marginTop: 300, color: '#ccc'}}>这里什么也没有</Text>
//       </View>
//     );
//   }
// }

import React, {Component} from 'react';
import {
  Text,
  Image,
  View,
  ActivityIndicator,
  TouchableHighlight,
  FlatList,
  StyleSheet,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
const styles = StyleSheet.create({
  movieTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#555',
  },
  movieContent: {
    fontSize: 13,
    color: '#666',
  },
});

export default class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      nowPage: 1,
      totalPage: 0,
      pageSize: 15,
      isloading: true,
    };
  }
  render() {
    return <View>{this.renderList()}</View>;
  }
  // 获取电影列表
  getMovieList = () => {
    // const url = `/component/testdata/top250.json`;
    // axios.get(url)
    //   .then(res => {
    //     console.log(res.data);
    const start = (this.state.nowPage - 1) * this.state.pageSize;
    const end = start + this.state.pageSize;
    const total = require('../testdata/in_theaters.json').total;
    const data = require('../testdata/in_theaters.json').subjects.slice(
      start,
      end,
    );
    //     this.setState({
    //       isLoading: false,
    //       movies: res.data.subjects,
    //       totalPage: Math.ceil(res.data.total / this.state.pageSize),
    //     });
    //   });
    setTimeout(() => {
      this.setState({
        isloading: false,
        movies: this.state.movies.concat(data),
        totalPage: Math.ceil(total / this.state.pageSize),
      });
    }, 1000);
  };
  componentDidMount() {
    this.getMovieList();
  }
  renderList = () => {
    if (this.state.isloading) {
      return (
        <View>
          <ActivityIndicator color="#FB7299" size="large"></ActivityIndicator>
        </View>
      );
    }
    return (
      <FlatList
        data={this.state.movies}
        renderItem={({item}) => this.renderItem(item)}
        keyExtractor={item => item.id}
        ItemSeparatorComponent={this.renderSeparator}
        onEndReachedThreshold={0.8}
        onEndReached={this.loadNextPages}
      />
    );
  };
  renderItem = item => {
    return (
      <TouchableHighlight
        underlayColor="#fff"
        onPress={() => {
          Actions.moviedetail({id: item.id});
        }}>
        <View style={{flexDirection: 'row', padding: 10}}>
          <Image
            source={{uri: item.cover}}
            style={{width: 130, height: 180, marginRight: 10}}></Image>
          <View style={{justifyContent: 'space-around'}}>
            <Text>
              <Text style={styles.movieTitle}>电影名称:</Text>
              <Text style={styles.movieContent}>{item.title}</Text>
            </Text>
            <Text>
              <Text style={styles.movieTitle}>播放量:</Text>
              <Text style={styles.movieContent}>{item.id}</Text>
            </Text>
            <Text style={{width: 250}}>
              <Text style={styles.movieTitle}>豆瓣地址:</Text>
              <Text style={styles.movieContent}>{item.url}</Text>
            </Text>
            <Text>
              <Text style={styles.movieTitle}>电影评分:</Text>
              <Text style={styles.movieContent}>{item.rate}</Text>
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  };
  renderSeparator = () => {
    return (
      <View
        style={{
          borderTopColor: '#ddd',
          borderTopWidth: 1,
          marginLeft: 10,
          marginRight: 10,
        }}></View>
    );
  };
  loadNextPages = () => {
    if (this.state.nowPage + 1 > this.state.totalPage) {
      return;
    }
    this.setState(
      {
        nowPage: this.state.nowPage + 1,
      },
      this.getMovieList,
    );
  };
}
