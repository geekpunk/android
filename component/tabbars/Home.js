import React, {Component} from 'react';
import {Text, View, StyleSheet,Image,TouchableHighlight} from 'react-native';
//导入轮播图组件
import Swiper from 'react-native-swiper';
import {Actions} from 'react-native-router-flux'
//样式
const styles = StyleSheet.create({
  box:{
    width:'33.33%',
    alignItems:'center',
    marginTop:30,
  }
});


export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lunbotu: [],
    };
  }
  //获取图片
  componentDidMount() {
    fetch(
      'https://api.dongmanxingkong.com/suijitupian/acg/1080p/index.php?return=json',
    )
      .then(res => res.json())
      .then(data => {
        this.setState({
          lunbotu: [
            {
              img: data.imgurl,
            },
            {
              img: 'https://tvax2.sinaimg.cn/large/0072Vf1pgy1foxli9lpyoj31kw0w0qlm.jpg',
            },
            {
              img: 'https://tvax1.sinaimg.cn/large/0072Vf1pgy1foxkbylysqj31kw0w0nmh.jpg',
            },
          ],
        });
      });
  }

  goMovieList=()=>{
    Actions.movielist()
  }
  render() {
    return (
      <View style={{height: '100%', backgroundColor: '#fff'}}>
        <View style={{height: 220}}>
          <Swiper
            key={this.state.lunbotu.length}
            style={styles.wrapper}
            autoplay={true}
            loop={true}>
            {this.state.lunbotu.map((item, i) => {
              return (
                <View key={i}>
                  <Image
                    source={{uri: item.img}}
                    style={{width: '100%', height: '100%'}}></Image>
                </View>
              );
            })}
          </Swiper>
        </View>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          <View style={styles.box}>
            <Image
              style={{width: 50, height: 50}}
              source={require('../../images/menu1.png')}></Image>
            <Text>新闻资讯</Text>
          </View>
          <View style={styles.box}>
            <Image
              style={{width: 50, height: 50}}
              source={require('../../images/menu2.png')}></Image>
            <Text>图片分享</Text>
          </View>
          <View style={styles.box}>
            <Image
              style={{width: 50, height: 50}}
              source={require('../../images/menu3.png')}></Image>
            <Text>商品购买</Text>
          </View>
          <View style={styles.box}>
            <Image
              style={{width: 50, height: 50}}
              source={require('../../images/menu4.png')}></Image>
            <Text>视频专区</Text>
          </View>
          {/* 只能放唯一的元素 */}
          <TouchableHighlight
            style={styles.box}
            onPress={this.goMovieList}
            underlayColor="#fff">
            <View>
              <Image
                style={{width: 50, height: 50}}
                source={require('../../images/menu5.png')}></Image>
              <Text>热映电影</Text>
            </View>
          </TouchableHighlight>

          <View style={styles.box}>
            <Image
              style={{width: 50, height: 50}}
              source={require('../../images/menu6.png')}></Image>
            <Text>联系我们</Text>
          </View>
        </View>
      </View>
    );
  }
}
