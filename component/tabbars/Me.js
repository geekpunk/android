import React, {Component} from 'react';
import {Button, View, Image} from 'react-native';
import ImagePicker from 'react-native-image-picker'; 

const photoOptions = {
  title: '选择图片',
  cancelButtonTitle: '取消',
  takePhotoButtonTitle: '拍照',
  chooseFromLibraryButtonTitle: '图片库',
  quality: 0.75,
  allowsEditing: true,
  maxWidth: 600,
  maxHeight: 600,
  noData: false,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
}; 

export default class Me extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgURL:
        'https://dss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2306696130,3636777462&fm=26&gp=0.jpg',
    };
  }
  render() {
    return (
      <View style={{alignItems: 'center', paddingTop: 70}}>
        <Image
          style={{marginBottom:50, width: 100, height: 100, borderRadius: 100}}
          source={{uri: this.state.imgURL}}></Image>
        <Button
          title="拍照"
          color="#FB7299"
          onPress={this.cameraAction}></Button>
      </View>
    );
  }
  cameraAction=()=>{
    ImagePicker.showImagePicker(photoOptions, (response) => {
      if (response.didCancel) {
        return
      }
      this.setState({
        imgURL: response.uri,
      });
      console.log(response);
    });
  }
}
