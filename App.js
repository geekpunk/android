/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import Home from './component/tabbars/Home';
import Me from './component/tabbars/Me';
import Search from './component/tabbars/Search';
import ShopCar from './component/tabbars/ShopCar';
//导入tabbar组件
//导入图标组件
import Icon from 'react-native-vector-icons/FontAwesome';
import TabNavigator from 'react-native-tab-navigator';
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'Home',
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FB7299" />
        <TabNavigator>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'Home'}
            title="Home"
            renderIcon={() => <Icon name="home" size={30} color="#ccc" />}
            renderSelectedIcon={() => (
              <Icon name="home" size={30} color="#0079ff" />
            )}
            onPress={() => this.setState({selectedTab: 'Home'})}>
            <Home />
          </TabNavigator.Item>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'Search'}
            title="Search"
            renderIcon={() => <Icon name="search" size={30} color="#ccc" />}
            renderSelectedIcon={() => (
              <Icon name="search-plus" size={30} color="#0079ff" />
            )}
            onPress={() => this.setState({selectedTab: 'Search'})}>
            <Search />
          </TabNavigator.Item>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'ShopCar'}
            title="ShopCar"
            renderIcon={() => (
              <Icon name="shopping-bag" size={28} color="#ccc" />
            )}
            renderSelectedIcon={() => (
              <Icon name="shopping-bag" size={28} color="#0079ff" />
            )}
            badgeText="1"
            onPress={() => this.setState({selectedTab: 'ShopCar'})}>
            <ShopCar />
          </TabNavigator.Item>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'me'}
            title="Me"
            renderIcon={() => <Icon name="user-o" size={28} color="#ccc" />}
            renderSelectedIcon={() => (
              <Icon name="user" size={28} color="#0079ff" />
            )}
            onPress={() => this.setState({selectedTab: 'me'})}>
            <Me />
          </TabNavigator.Item>
        </TabNavigator>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
