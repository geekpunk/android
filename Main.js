//项目根组件
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import App from './App'
import MovieList from './component/movie/MovieList'
import MovieDetail from './component/movie/MovieDetail'
//导入路由组件
import {Router, Stack, Scene} from 'react-native-router-flux'
// Router: 就相当于 昨天我们所学的  HashRouter
// Stack: 这是一个分组的容器，他不表示具体的路由，专门用来给路由分组的
// Scene：就表示一个具体的路由规则，好比 昨天学到的 Route
export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'App',
    };
  }

  render() {
    return (
      <Router sceneStyle={{color: '#fff'}}>
        <Stack key="root">
          {/* 配置路由规则 */}
          {/* 注意，所有的路由规则，都应该写到这个位置 */}
          {/* 第一个 Scene 就是默认要展示的首页 */}
          {/* key 属性，表示路由的规则名称，将来可以使用这个 key ，进行编程式导航，每一个路由规则，都应该提供一个 唯一的key， key不能重复 */}
          <Scene
            key="app"
            component={App}
            navigationBarStyle={{backgroundColor: '#FB7299', height: 40}}
            titleStyle={{fontSize: 15, color: '#fff', textAlign: 'center'}}
            title={this.state.selectedTab}
          />
          {/* 电影列表路由规则 */}
          <Scene
            key="movielist"
            component={MovieList}
            backButtonTintColor="#fff"
            backButtonTextStyle={{color: '#fff'}}
            navigationBarStyle={{backgroundColor: '#FB7299', height: 40}}
            titleStyle={{fontSize: 15, color: '#fff', marginLeft: 77}}
            title="热映电影列表"></Scene>
          <Scene
            key="moviedetail"
            component={MovieDetail}
            backButtonTintColor="#fff"
            backButtonTextStyle={{color: '#fff'}}
            navigationBarStyle={{backgroundColor: '#FB7299', height: 40}}
            titleStyle={{fontSize: 15, color: '#fff', marginLeft: 92}}
            title="电影详情"></Scene>
        </Stack>
      </Router>
    );
  }
}
